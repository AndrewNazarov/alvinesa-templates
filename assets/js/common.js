/**
 * Created by dron on 30.04.2016.
 */

var carouselHomeInit = 0;

!function ($) {
    $(window).on('load', function() {
        // initialize global-menu on top if page is scroll
        menuOnTop();

        // initialize inner-page tabType for mobile/screen
        tabType();

        // initialize background video for screen
        var video = $('#home-bg-area-video')[0];
        if(window.innerWidth > 768) {
            video.setAttribute('preload', 'auto');
            video.autoplay = true;
        }

        $('#carousel-home').carousel('pause');

        // carousel initialize
        $('[data-slide-start]').on('click', function() {
            carouselHomeInit = 1;
            var obj = $('[data-slide-start]');
            setTimeout(function(){
                obj.each(function(i){
                    obj[i].style.display = 'none';
                });
                $('.control-hide').removeClass('control-hide');
                $('#carousel-home .end').removeClass('end');
            }, 600);

            if($(this).attr('data-slide-start') == 'next') {
                obj.addClass('go-next');
                $('#carousel-home .start').removeClass('start');
            }
            else if($(this).attr('data-slide-start') == 'prev') {
                obj.addClass('go-prev');
                $('#carousel-home .start').removeClass('start');
                $('#carousel-home').carousel('cycle');
            }
        });
    });

    $(window).scroll(function() {
        menuOnTop();
    });

    $(window).resize(function(){
        tabType();
    });

    // global-menu: toggle on mobile <ul class="inner-ul">
    $('.menu-global .inner > a').on('click', function(event) {
        var el = $(event.target.closest('.inner'));

        if(!el.hasClass('open')) {
            var allEl = $('.menu-global .inner');
            allEl.each(function(n){
                $(allEl[n]).removeClass('open');
            });
            el.addClass('open');
        }
        else {
            el.removeClass('open');
        }
        return false;
    });

    var videoPlay = 1;

    $('.btn-video-play').on('click', function(){
        var video = $('#home-bg-area-video')[0];
        console.log(video);
        if(videoPlay) {
            videoPlay = 0;
            video.pause();
        }
        else {
            videoPlay = 1;
            video.play();
        }
        return false;
    });



}(window.jQuery);

// menuOnTop() - initialize global-menu on top if page is scroll
function menuOnTop() {
    if(window.innerWidth > 768) {
        if($(this).scrollTop() > 50) {
            $('body').addClass('notTop');
        } else {
            $('body').removeClass('notTop');
        }
    }
}

// tabType() - initialize inner-page tabType for mobile/screen
function tabType() {
    var tabWrap = $('#tab-type');

    if(window.innerWidth > 768) {
        if(!tabWrap.hasClass('tab-content')) {
            tabWrap.removeClass('panel').addClass('tab-content');
        }
    }
    else {
        if(!tabWrap.hasClass('panel')) {
            tabWrap.removeClass('tab-content').addClass('panel');
        }
    }
}

function menuGlobalOpen(menu) {
    var menuGlobal = $('.' + menu);
    if(!menuGlobal.hasClass('menu-visible')) menuGlobal.addClass('menu-visible');
    return false;
}

function menuGlobalClose(menu) {
    var menuGlobal = $('.' + menu);
    if(menuGlobal.hasClass('menu-visible')) menuGlobal.removeClass('menu-visible');
    return false;
}

var cssFix = function(){
    var u = navigator.userAgent.toLowerCase(),
        is = function(t){return (u.indexOf(t)!=-1)};
    $("html").addClass([
        (!(/opera|webtv/i.test(u))&&/msie (\d)/.test(u))?('ie ie'+RegExp.$1)
            :is('firefox/2')?'gecko ff2'
            :is('firefox/3')?'gecko ff3'
            :is('gecko/')?'gecko'
            :is('opera/9')?'opera opera9':/opera (\d)/.test(u)?'opera opera'+RegExp.$1
            :is('konqueror')?'konqueror'
            :is('applewebkit/')?'webkit safari'
            :is('mozilla/')?'gecko':'',
        (is('x11')||is('linux'))?' linux'
            :is('mac')?' mac'
            :is('win')?' win':''
    ].join(''));
}();