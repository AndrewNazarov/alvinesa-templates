/**
 * Created by dron on 03.05.2016.
 */
//init webshims before dom ready
$.webshims.setOptions('loadStyles', true);
$.webshims.setOptions('forms', {
    //Use customizeable validation messages
    customMessages: true,
    //replace validation UI in all browsers
    replaceValidationUI: true
});

$.webshims.polyfill('es5 forms forms-ext');