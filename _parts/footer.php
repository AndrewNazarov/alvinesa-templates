<footer id="page-footer" class="page-footer">
    <div class="container">
        <div class="logo-footer">
            <div class="hr pull-left"></div>
            <div class="hr pull-right"></div>
            <img src="assets/img/logo-footer.png">
        </div>
        <div class="row">
            <div class="col-sm-3 col-sm-push-3">
                <p class="title">Productos</p>
                <ul class="list-unstyled">
                    <li><a href="#" target="_blank">Alcoholes de uso en boca</a></li>
                    <li><a href="#" target="_blank">Alcoholes industriales y energ?ticos</a></li>
                    <li><a href="#" target="_blank">?cido tart?rico</a></li>
                    <li><a href="#" target="_blank">Pepita de uva</a></li>
                    <li><a href="#" target="_blank">Antocianos y polifenoles</a></li>
                </ul>
                <div class="hr hidden-sm hidden-md hidden-lg"></div>
            </div>
            <div class="col-sm-3 col-sm-push-3">
                <p class="title">Productos y soluciones para:</p>
                <ul class="list-unstyled">
                    <li><a href="#" target="_blank">Fabricantes de alcohol sanitario</a></li>
                    <li><a href="#" target="_blank">Biocombustibles</a></li>
                    <li><a href="#" target="_blank">Empresas industriales</a></li>
                    <li><a href="#" target="_blank">Fabricantes de piensos para animales</a></li>
                    <li><a href="#" target="_blank">F?bricas de licores</a></li>
                    <li><a href="#" target="_blank">Bodegas</a></li>
                </ul>
                <div class="hr hidden-sm hidden-md hidden-lg"></div>
            </div>
            <div class="col-sm-3 col-sm-push-3">
                <p class="title">Suscr?bete a nuestro bolet?n:</p>
                <p>Recibir?s informaci?n ?til sobre nosotros y sobre el sector</p>
                <form class="form-send">
                    <input type="email" placeholder="E-mail">
                    <button class="btn-alvinesa" type="submit">Suscribirme</button>
                </form>
                <div class="hr"></div>
            </div>
            <div class="contacts col-sm-3 col-sm-pull-9">
                <p class="title">Contacto</p>
                <p>Cra. Daimiel-Valdepe?as Km 4,8 Pol. Ind.
                    <br/>"El Campillo"
                    <br/>Daimiel (Ciudad Real). Espa?a.
                </p>
                <p>
                    Tel?fono: (+34) 926 26 06 70
                    <br/>Email: <a href="mailto:alvinesa@alvinesa.com">alvinesa@alvinesa.com</a>
                </p>
                <div class="hr hidden-sm hidden-md hidden-lg"></div>
            </div>
        </div>
        <div class="row">
            <div class="privacy-links col-sm-3 col-sm-push-9">
                <a href="#" target="_blank">Aviso legal</a>
                <a href="#" target="_blank">Pol?tica de privacidad</a>
            </div>
        </div>
    </div>
</footer>

<script src="assets/js/vendor/jquery-1.12.0.min.js"></script>

<script src="assets/js/vendor/webshims/minified/polyfiller.js"></script>

<script src="assets/js/vendor/bootstrap.min.js"></script>
<script src="assets/js/vendor/carousel.js"></script>