
<header id="page-header" class="header-page">
    <div class="container">
        <i class="menu-global-open" onclick="menuGlobalOpen('menu-global-wrap');"></i>
        <div class="row">
            <a class="logo-header col-xs-8 col-xs-push-2 col-sm-2 col-sm-push-0 pull-left" href="#"><img src="assets/img/alvinesa-logo.png" alt="Alvinesa" title="Alvinesa"></a>
            <div class="pull-right localisation col-sm-4 col-md-2 ta-r">
                <a href="#">English<span class="hidden-xs"> version</span></a>
            </div>
            <div class="col-sm-10 menu-global-block">
                <nav class="pull-right menu-global-wrap">
                    <div class="menu-global-close-wrap">
                        <i class="menu-global-close" onclick="menuGlobalClose('menu-global-wrap');"></i>
                    </div>
                    <div class="menu-global-bg"></div>
                    <ul class="menu-global" id="menu-global">
                        <li><a class="active" href="#"><span>Inicio</span></a></li>
                        <li class="inner"><a href="#"><span>Productos</span><i class="caret"></i></a>
                            <ul class="inner-ul list-unstyled">
                                <li><a href="#">Todos los productos</a></li>
                                <li><a href="#">Alcoholes de uso en boca</a></li>
                                <li><a href="#">Alcoholes industriales y energ?ticos</a></li>
                                <li><a href="#">?cido tart?rico</a></li>
                                <li><a href="#">Pepita de uva</a></li>
                                <li><a href="#">Antocianos y polifenoles</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><span>Aplicaciones</span></a></li>
                        <li class="inner"><a href="#"><span>?A qui?n nos dirigimos?</span><i class="caret"></i></a>
                            <ul class="inner-ul list-unstyled">
                                <li><a href="#">Bodegas</a></li>
                                <li><a class="active" href="#">F?bricas de licores</a></li>
                                <li><a href="#">Fabricantes de pienso para animales</a></li>
                                <li><a href="#">Fabricantes de alimentaci?n</a></li>
                                <li><a href="#">Empresas industriales</a></li>
                                <li><a href="#">Biocombustibles</a></li>
                                <li><a href="#">Fabricantes de alcohol sanitario</a></li>
                            </ul>
                        </li>
                        <li class="inner"><a href="#"><span>Sobre nosotros</span><i class="caret"></i></a>
                            <ul class="inner-ul list-unstyled">
                                <li><a href="#">Bodegas</a></li>
                                <li><a href="#">F?bricas de licores</a></li>
                                <li><a href="#">Fabricantes de pienso para animales</a></li>
                                <li><a href="#">Fabricantes de alimentaci?n</a></li>
                                <li><a href="#">Empresas industriales</a></li>
                                <li><a href="#">Biocombustibles</a></li>
                                <li><a href="#">Fabricantes de alcohol sanitario</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><span>Blog</span></a></li>
                        <li><a href="#"><span>Contacto</span></a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="hr hidden-sm hidden-md hidden-lg"></div>
    </div>
</header>