<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Alvinesa</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="assets/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Lora' rel='stylesheet' type='text/css'>

    <script src="assets/js/vendor/modernizr-2.6.2.min.js"></script>
    <!--[if lte IE 8]><script src="assets/js/vendor/respond.min.js"></script><![endif]-->
</head>
<body>

<!--HEADER-->
<?php include('_parts/header.php'); ?>
<!--!HEADER-->

    <main>
        <section class="inner-bg-img withImg">
            <div class="content container">
                <h2>Copy de página interior, para producto <span class="br"></span>u otra sección</h2>
                <h4>Subtítulo para incluir algún texto adicional al título del copy, que se <span class="br"></span>utilizará en páginas interiores</h4>
            </div>
        </section>
        <section class="about-technology-inner ta-c container">
            <h1>H1: Título para introducir un bloque de contenido</h1>
            <div class="row about-inner-block-0">
                <div class="col-sm-3">
                    <div class="img-wrapper"><i class="icon icon-1"></i></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat.</p>
                </div>
                <div class="col-sm-3">
                    <div class="img-wrapper"><i class="icon icon-2"></i></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat.</p>
                </div>
                <div class="col-sm-3">
                    <div class="img-wrapper"><i class="icon icon-3"></i></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat.</p>
                </div>
                <div class="col-sm-3">
                    <div class="img-wrapper"><i class="icon icon-4"></i></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat.</p>
                </div>
            </div>
            <h2>H2: Título para introducir un bloque de contenido</h2>
            <div class="row about-inner-block-1">
                <div class="col-sm-3">
                    <div class="img-wrapper"><img src="assets/img/inner/inner-block1-1.jpg"></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat.</p>
                </div>
                <div class="col-sm-3">
                    <div class="img-wrapper"><img src="assets/img/inner/inner-block1-2.jpg"></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat.</p>
                </div>
                <div class="col-sm-3">
                    <div class="img-wrapper"><img src="assets/img/inner/inner-block1-3.jpg"></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat.</p>
                </div>
                <div class="col-sm-3">
                    <div class="img-wrapper"><img src="assets/img/inner/inner-block1-4.jpg"></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat.</p>
                </div>
            </div>
        </section>
        <section class="industry-news inner-page-news container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="news-items" id="news-items">
                        <ul class="tab-links list-inline">
                            <li class="active"><a href="#block-1-1" data-toggle="tab"><span>Bloque uno</span></a></li>
                            <li><a href="#block-1-2" data-toggle="tab"><span>Usos del producto</span></a></li>
                            <li><a href="#block-1-3" data-toggle="tab"><span>Otro bloque más</span></a></li>
                        </ul>

                        <div id="tab-type" class="panel">
                            <a class="collapse-link" data-toggle="collapse" data-parent="#news-items" href="#block-1-1"><span>Bloque uno</span><i class="caret"></i></a>
                            <div class="panel-collapse collapse tab-pane fade in active" id="block-1-1">
                                <article class="row">
                                    <div class="img-wrapper col-sm-9-4">
                                        <img src="assets/img/inner/inner-block2-1.jpg">
                                    </div>
                                    <div class="col-sm-9-5">
                                        <a class="title" href="#" target="_blank">
                                            Título lorem ipsum dolor sit amet, consectetur adipiscing elit
                                        </a>
                                        <div class="description">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.
                                                <br/>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucis arcu condimentum sed.
                                            </p>
                                        </div>
                                    </div>
                                </article>
                                <article class="row">
                                    <div class="img-wrapper col-sm-9-4">
                                        <img src="assets/img/inner/inner-block2-2.jpg">
                                    </div>
                                    <div class="col-sm-9-5">
                                        <a class="title" href="#" target="_blank">
                                            Título lorem ipsum dolor sit amet, consectetur adipiscing elit
                                        </a>
                                        <div class="description">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.
                                                <br/>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucis arcu condimentum sed.
                                            </p>
                                        </div>
                                    </div>
                                </article>
                                <article class="row">
                                    <div class="img-wrapper col-sm-9-4">
                                        <img src="assets/img/inner/inner-block2-3.jpg">
                                    </div>
                                    <div class="col-sm-9-5">
                                        <a class="title" href="#" target="_blank">
                                            Título lorem ipsum dolor sit amet, consectetur adipiscing elit
                                        </a>
                                        <div class="description">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.
                                                <br/>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucis arcu condimentum sed.
                                            </p>
                                        </div>
                                    </div>
                                </article>
                            </div>

                            <a class="collapse-link collapsed" data-toggle="collapse" data-parent="#news-items" href="#block-1-2"><span>Usos del producto</span><i class="caret"></i></a>
                            <div class="panel-collapse collapse tab-pane fade" id="block-1-2">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.
                                </p>
                            </div>

                            <a class="collapse-link collapsed" data-toggle="collapse" data-parent="#news-items" href="#block-1-3"><span>Otro bloque más</span><i class="caret"></i></a>
                            <div class="panel-collapse collapse tab-pane fade" id="block-1-3">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="panel hidden-xs">
                        <a class="collapse-link" data-toggle="collapse" data-parent="#news-items" href="#block-2-1"><span>Panel uno</span><i class="caret"></i></a>
                        <div class="panel-collapse collapse tab-pvcane fade in" id="block-2-1">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.
                            </p>
                        </div>

                        <a class="collapse-link collapsed" data-toggle="collapse" data-parent="#news-items" href="#block-2-2"><span>Panel dos</span><i class="caret"></i></a>
                        <div class="panel-collapse collapse tab-pane fade" id="block-2-2">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.
                            </p>
                        </div>

                        <a class="collapse-link collapsed" data-toggle="collapse" data-parent="#news-items" href="#block-2-3"><span>Panel tres</span><i class="caret"></i></a>
                        <div class="panel-collapse collapse tab-pane fade" id="block-2-3">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.
                            </p>
                        </div>
                    </div>
                </div>
                <aside class="inner-baners col-sm-3">
                    <div class="violet-banner">
                        <p>Esto es un texto con una llamada a la acción debajo, y que incluye una lista de viñetas.
                            Está incluido en un bloque usado para destacarlo.
                        </p>
                        <ul class="list-alvinesa list-unstyled">
                            <li>Una de las opciones</li>
                            <li>Otra de las opciones</li>
                            <li>Y alguna más</li>
                        </ul>
                        <a class="btn-alvinesa">CTA del bloque destacado</a>
                    </div>
                    <div class="white-banner">
                        <p>Esto es un texto con una llamada a la acción debajo, y que incluye una lista de viñetas.
                            Está incluido en un bloque usado para destacarlo.
                        </p>
                        <ul class="list-alvinesa list-unstyled">
                            <li>Una de las opciones</li>
                            <li>Otra de las opciones</li>
                            <li>Y alguna más</li>
                        </ul>
                        <a class="btn-alvinesa">CTA del bloque destacado</a>
                    </div>
                </aside>
                <div class="col-sm-9">
                    <div class="panel hidden-sm hidden-md hidden-lg">
                        <a class="collapse-link" data-toggle="collapse" data-parent="#news-items" href="#block-2-1"><span>Panel uno</span><i class="caret"></i></a>
                        <div class="panel-collapse collapse tab-pvcane fade in" id="block-2-1">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.
                            </p>
                        </div>

                        <a class="collapse-link collapsed" data-toggle="collapse" data-parent="#news-items" href="#block-2-2"><span>Panel dos</span><i class="caret"></i></a>
                        <div class="panel-collapse collapse tab-pane fade" id="block-2-2">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.
                            </p>
                        </div>

                        <a class="collapse-link collapsed" data-toggle="collapse" data-parent="#news-items" href="#block-2-3"><span>Panel tres</span><i class="caret"></i></a>
                        <div class="panel-collapse collapse tab-pane fade" id="block-2-3">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

<!--FOOTER-->
<?php include('_parts/footer.php'); ?>
<!--FOOTER-->

    <script src="assets/js/common.js"></script>
    <script src="assets/js/common-init.js"></script>
</body>
</html>