<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Alvinesa</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="assets/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Lora' rel='stylesheet' type='text/css'>

    <script src="assets/js/vendor/modernizr-2.6.2.min.js"></script>
    <!--[if lte IE 8]><script src="assets/js/vendor/respond.min.js"></script><![endif]-->
</head>
<body>

<!--HEADER-->
<?php include('_parts/header.php'); ?>
<!--!HEADER-->

    <main>
        <section class="home-bg-area">
            <video class="hidden-xs" id="home-bg-area-video" loop="loop" poster="assets/img/temp/DJI_0036.jpg" muted>
                <source src="assets/file/DJI_0036.mp4" type="video/mp4">
                <source src="assets/file/DJI_0036.ogv" type="video/ogg">
                <source src="assets/file/DJI_0036.webm" type="video/webm">
            </video>
            <div class="content container">
                <h2>Producción de alcoholes y <span class="br"></span>subproductos del vino desde 1993</h2>
                <h4><span>Número uno en Europa en producción</span>, exportando a los 5 continentes </h4>
                <a class="btn-alvinesa active" href="#" target="_blank">Nuestros productos</a>
                <a class="btn-alvinesa" href="#" target="_blank">Conócenos</a>
                <a class="btn-video-play hidden-xs"><span class="play-icon"><i></i></span>Ver vídeo</a>
            </div>
        </section>
        <section class="home-slider container">
            <h3 class="hidden-xs">Descubre nuestros productos y sus usos</h3>
            <p class="description hidden-xs">Conoce los posibles usos de nuestros moviendo el tirador encima de la imagen</p>
            <div id="carousel-home" class="carousel slide" data-ride="carousel-new">
                <div class="carousel-inner">
                    <div class="item">
                        <img src="assets/img/home-slider/home-slider-1.png">
                    </div>
                    <div class="item active start end">
                        <img src="assets/img/home-slider/home-slider-2.png">
                    </div>
                    <div class="item">
                        <img src="assets/img/home-slider/home-slider-1.png">
                    </div>
                    <div class="item">
                        <img src="assets/img/home-slider/home-slider-2.png">
                    </div>
                    <div class="item">
                        <img src="assets/img/home-slider/home-slider-1.png">
                    </div>
                    <div class="item">
                        <img src="assets/img/home-slider/home-slider-2.png">
                    </div>
                </div>
                <ul class="carousel-list list-inline hidden-xs carousel-indicators">
                    <li data-target="#carousel-home" data-slide-to="0"><a>Alcohol neutro</a></li>
                    <li data-target="#carousel-home" data-slide-to="1" class="active"><a>Acido tartarico</a></li>
                    <li data-target="#carousel-home" data-slide-to="2"><a>Harina de pepitas de uva</a></li>
                    <li data-target="#carousel-home" data-slide-to="3"><a>Pepitas de uva</a></li>
                    <li data-target="#carousel-home" data-slide-to="4"><a>Alcohol bruto</a></li>
                    <li data-target="#carousel-home" data-slide-to="5"><a>Aguardientes</a></li>
                </ul>
                <a class="left carousel-control temp-control hidden-xs" data-slide-start="prev"></a>
                <a class="right carousel-control temp-control hidden-xs" href="#carousel-home" data-slide="next" data-slide-start="next"></a>

                <a class="left carousel-control control-hide hidden-xs" href="#carousel-home" data-slide="prev"></a>
                <a class="right carousel-control control-hide hidden-xs" href="#carousel-home" data-slide="next"></a>
            </div>
        </section>
        <section class="about-technology container">
            <h3>Sectores a los que vamos dirigidos</h3>
            <div class="row">
                <div class="col-sm-3">
                    <h4>Bebidas y alimentación</h4>
                    <ul class="list-unstyled">
                        <li>Bodegas</li>
                        <li>Licores</li>
                        <li>Tartárico</li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h4>Industria y energía</h4>
                    <ul class="list-unstyled">
                        <li>Industria</li>
                        <li>Biocombustibles</li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h4>Alimentación animal</h4>
                    <ul class="list-unstyled">
                        <li>Productos para fabricación de piensos animales</li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h4>Productos sanitarios</h4>
                    <ul class="list-unstyled">
                        <li>Alcoholes sanitarios</li>
                    </ul>
                </div>
            </div>
            <h3>Número uno en Europa</h3>
            <div class="row">
                <div class="col-sm-3">
                    <h4>Sobre las instalaciones</h4>
                    <ul class="list-unstyled">
                        <li>Extensión</li>
                        <li>Planta de producción</li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h4>Volumen de compra</h4>
                    <ul class="list-unstyled">
                        <li>Mayor cantidad de materia prima disponible</li>
                        <li>Precios más competitivos</li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h4>Logística propia</h4>
                    <ul class="list-unstyled">
                        <li>Disponibilidad del producto y rapidez en la entrega</li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h4>Investigación y desarrollo</h4>
                    <ul class="list-unstyled">
                        <li>Proyectos Clamber, Primicia y Bioerred</li>
                    </ul>
                </div>
            </div>
            <div class="row bg-planet-map">
                <div class="container">
                    <h2>Exportamos a más de 15 países <br/>en los 5 continentes</h2>
                    <p class="ta-c">Llevamos nuestros productos a cualquier rincón del mundo</p>
                    <div class="sales-department">
                        <h2>Nuestro departamento comercial <span class="br"></span>y técnico a su disposición</h2>
                        <center>
                            <a class="btn-alvinesa active" href="#" target="_blank">Contáctenos</a>
                            <a class="btn-alvinesa" href="#" target="_blank">¿Le llamamos?</a>
                        </center>
                    </div>
                </div>
            </div>
        </section>
        <section class="industry-news container">
            <h3>Noticias del sector</h3>
            <div class="news-items">
                <article class="row">
                    <div class="img-wrapper col-sm-3">
                        <img src="assets/img/home/noticias-1.jpg">
                    </div>
                    <div class="col-sm-6">
                        <a class="title" href="#" target="_blank">
                            Natac y Alvinesa trabajan en el desarrollo de nuevos aditivos naturales para alimentación
                        </a>
                        <div class="description">
                            <p>
                                Natac Biotech y Alvinesa Alcoholera Vinícola están trabajando en un proyecto de
                                investigación aplicada orientado al desarrollo de nuevos ingredientes innovadores de alto
                                valor añadido. En concreto, están desarrollando nuevos aditivos naturales (antioxidantes,
                                antimicrobianos y sazonadores) para alimentación humana.
                            </p>
                        </div>
                    </div>
                    <aside class="news-banners img-wrapper col-sm-3 hidden-xs">
                        <img src="assets/img/temp/banner.jpg">
                    </aside>
                </article>
                <article class="row">
                    <div class="img-wrapper col-sm-3">
                        <img src="assets/img/home/noticias-2.jpg">
                    </div>
                    <div class="col-sm-6">
                        <a class="title" href="#" target="_blank">
                            Natac y Alvinesa trabajan en el desarrollo de nuevos aditivos naturales para alimentación
                        </a>
                        <div class="description">
                            <p>
                                Natac Biotech y Alvinesa Alcoholera Vinícola están trabajando en un proyecto de
                                investigación aplicada orientado al desarrollo de nuevos ingredientes innovadores de alto
                                valor añadido. En concreto, están desarrollando nuevos aditivos naturales (antioxidantes,
                                antimicrobianos y sazonadores) para alimentación humana.
                            </p>
                        </div>
                    </div>
                    <aside class="news-banners img-wrapper col-sm-3 hidden-xs">
                        <img src="assets/img/temp/banner.jpg">
                    </aside>
                </article>
                <article class="row">
                    <div class="img-wrapper col-sm-3">
                        <img src="assets/img/home/noticias-3.jpg">
                    </div>
                    <div class="col-sm-6">
                        <a class="title" href="#" target="_blank">
                            Natac y Alvinesa trabajan en el desarrollo de nuevos aditivos naturales para alimentación
                        </a>
                        <div class="description">
                            <p>
                                Natac Biotech y Alvinesa Alcoholera Vinícola están trabajando en un proyecto de
                                investigación aplicada orientado al desarrollo de nuevos ingredientes innovadores de alto
                                valor añadido. En concreto, están desarrollando nuevos aditivos naturales (antioxidantes,
                                antimicrobianos y sazonadores) para alimentación humana.
                            </p>
                        </div>
                    </div>
                    <aside class="news-banners img-wrapper col-sm-3 hidden-xs">
                        <img src="assets/img/temp/banner.jpg">
                    </aside>
                </article>
            </div>

            <div class="news-banners row hidden-sm hidden-md hidden-lg">
                <div class="img-wrapper col-sm-4">
                    <img src="assets/img/temp/banner.jpg">
                </div>
                <div class="img-wrapper col-sm-4">
                    <img src="assets/img/temp/banner.jpg">
                </div>
                <div class="img-wrapper col-sm-4">
                    <img src="assets/img/temp/banner.jpg">
                </div>
            </div>
        </section>
    </main>

<!--FOOTER-->
<?php include('_parts/footer.php'); ?>
<!--FOOTER-->

    <script src="assets/js/common.js"></script>
    <script src="assets/js/common-init.js"></script>
</body>
</html>